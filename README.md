# 介绍

> 之前我很长一段时间都使用 `STM32CubeMX + Keil` 开发, 我承认 `Keil` 是个很好的软件, 我也很愿意继续时候使用 `Keil`. 但是后面搞其他的开发过程中发现很多项目没办法使用它 (ESP32, ESP8266,  Linux开发等).

- `STM32` 是当今最主流的 `MCU` 之一所以选择这个芯片
- `STM32CubeMX` `STM32` 最佳开方式, 采用图形化的方式配置
- `Makefile项目` 相对于 `Keil项目` 来说更适合开源
- `Keil软件` 不免费, 而且将来使用其他的芯片, `Keil` 就用不上了
- `Makefile` 不仅免费, 而且很多其他的嵌入式开发基本上都是吃 `Makefile`
- 如果 `Makefile` 用熟了, `Keil` `IAR` 这类的软件用起来也不会太难

---

## 目录说明

| 目录   | 说明         |
| ------ | ------------ |
| `doc`  | 存放项目文档 |
| `其他` | 存放项目工程 |

---

<div align=center><img width="1000" src="https://gitee.com/leafguo/image/raw/master/vx/vx.png"/></div>

---
