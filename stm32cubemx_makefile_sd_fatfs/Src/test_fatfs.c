#include "fatfs.h"

void test_fatfs(void)
{
    FATFS fs;
    FIL fil;

    uint32_t w_datas_num;                   // 写数据长度
    uint32_t r_datas_num;                   // 读数据长度
    uint8_t w_datas[] = "hello iotxiaohu";  // 写的数据
    uint8_t r_datas[100];                   // 存放读出来的数据
    char filen_name[] = "test_fatfs.txt";    // 文件名

    printf("\n ****** test fatfs ******\n\n");

    /* 注册文件系统*/
    retSD = f_mount(&fs, "", 0);
    if (retSD) {
        printf("> mount error : %d \n", retSD);
        return;
    }
    printf("> mount OK!!! \n");

    /* 创建并且打开文件 */
    retSD = f_open(&fil, filen_name, FA_CREATE_ALWAYS | FA_WRITE);
    if (retSD) {
        printf("> open file error : %d\n", retSD);
        return;
    }
    printf("> open file OK!!! \n");

    /* 写文件 */
    retSD = f_write(&fil, w_datas, sizeof(w_datas), (void *)&w_datas_num);
    if (retSD) {
        printf("> write file error : %d\n", retSD);
        return;
    }
    printf("> write file OK!!! \n");
    printf("> write Data : %s\n", w_datas);

    /* 关闭文件 */
    retSD = f_close(&fil);
    if (retSD) {
        printf("> close error : %d\n", retSD);
        return;
    }
    printf("> close OK!!! \n");

    /* 打开文件 */
    retSD = f_open(&fil, filen_name, FA_READ);
    if (retSD) {
        printf("> open file error : %d\n", retSD);
        return;
    }
    printf("> open file OK!!! \n");

    /* 读取文件 */
    retSD = f_read(&fil, r_datas, sizeof(r_datas), (UINT *)&r_datas_num);
    if (retSD) {
        printf("> read error!!! %d\n", retSD);
        return;
    }
    printf("> read OK!!! \n");
    printf("> read Data : %s\n", r_datas);

    /* 关闭文件 */
    retSD = f_close(&fil);
    if (retSD) {
        printf("> close error!!! %d\n", retSD);
        return;
    }
    printf("> close OK!!! \n");

    /* 比较读取的是否与写的一致 */
    if (r_datas_num == w_datas_num) {
        printf("> test fatfs OK!!!\n");
    }
}